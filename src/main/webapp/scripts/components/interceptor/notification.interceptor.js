 'use strict';

angular.module('travelSpellsApp')
    .factory('notificationInterceptor', function ($q, AlertService) {
        return {
            response: function(response) {
                var alertKey = response.headers('X-travelSpellsApp-alert');
                if (angular.isString(alertKey)) {
                    AlertService.success(alertKey, { param : response.headers('X-travelSpellsApp-params')});
                }
                return response;
            }
        };
    });
