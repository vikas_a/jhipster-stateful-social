/* globals $ */
'use strict';

angular.module('travelSpellsApp')
    .directive('travelSpellsAppPagination', function() {
        return {
            templateUrl: 'scripts/components/form/pagination.html'
        };
    });
