/* globals $ */
'use strict';

angular.module('travelSpellsApp')
    .directive('travelSpellsAppPager', function() {
        return {
            templateUrl: 'scripts/components/form/pager.html'
        };
    });
