/**
 * Data Access Objects used by WebSocket services.
 */
package com.travelspells.web.websocket.dto;
