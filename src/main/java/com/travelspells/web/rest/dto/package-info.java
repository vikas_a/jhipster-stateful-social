/**
 * Data Transfer Objects used by Spring MVC REST controllers.
 */
package com.travelspells.web.rest.dto;
